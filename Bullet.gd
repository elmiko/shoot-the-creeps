extends Area2D

signal bullet_hit(mob)

const velocity = 200.0

func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()

func _process(delta):
	position.y -= velocity * delta
	
func _on_body_entered(body):
	hide()
	bullet_hit.emit(body)
