# updating from standard tutorial

1. do the normal tutorial.

2. move player to bottom of screen, in StartPosition node, update y to `660`

3. restrict player to move left and right only

```
diff --git a/Player.gd b/Player.gd
index 338c556..739ea06 100644
--- a/Player.gd
+++ b/Player.gd
@@ -19,10 +19,6 @@ func _process(delta):
				velocity.x += 1
		if Input.is_action_pressed("move_left"):
				velocity.x -= 1
-       if Input.is_action_pressed("move_down"):
-               velocity.y += 1
-       if Input.is_action_pressed("move_up"):
-               velocity.y -= 1

		if velocity.length() > 0:
				velocity = velocity.normalized() * speed
@@ -38,10 +34,8 @@ func _process(delta):
				$AnimatedSprite2D.flip_v = false
				# See the note below about boolean assignment.
				$AnimatedSprite2D.flip_h = velocity.x < 0
-       elif velocity.y != 0:
+       else:
				$AnimatedSprite2D.animation = "up"
-               $AnimatedSprite2D.flip_v = velocity.y > 0
-

 func _on_body_entered(body):
		hide() # Player disappears after being hit.
```

4. make mobs face downward only

```
diff --git a/Main.gd b/Main.gd
index 4527c44..ad782d3 100644
--- a/Main.gd
+++ b/Main.gd
@@ -31,14 +31,11 @@ func _on_mob_timer_timeout():
		var mob_spawn_location = get_node("MobPath/MobSpawnLocation")
		mob_spawn_location.progress_ratio = randf()

-       # Set the mob's direction perpendicular to the path direction.
-       var direction = mob_spawn_location.rotation + PI / 2
-
		# Set the mob's position to a random location.
		mob.position = mob_spawn_location.position

-       # Add some randomness to the direction
-       direction += randf_range(-PI / 4, PI / 4)
+       # Set the mob's rotation to face the bottom of the window
+       var direction = PI / 2
		mob.rotation = direction

		# Choose the velocity for the mob.
```

5. remove points from mob spawn path so that only the top left and top right points are left

6. create bullet scene

7. connect bullet to fire event

8. add collision detection and bullet/mob removal, set collision layer to 2

9. update score mechanism
