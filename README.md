# Shoot the Creeps

This is a project to expand the [Godot engine basic 2D tutorial][tutorial]
to change functionality from dodging to shooting.

See the [associated blog post "From Dodging to Shooting in Godot"][blog] for
a detailed breakdown.

[tutorial]: https://docs.godotengine.org/en/stable/getting_started/first_2d_game/index.html
[blog]: https://notes.elmiko.dev/2023/08/26/from-dodging-to-shooting.html
