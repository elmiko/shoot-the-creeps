extends Node

@export var bullet_scene: PackedScene
@export var mob_scene: PackedScene
var score
var bullet_cooldown = false


func game_over():
	$Music.stop()
	$DeathSound.play()
	$MobTimer.stop()
	$HUD.show_game_over()
	
func new_game():
	get_tree().call_group("mobs", "queue_free")
	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$HUD.update_score(score)
	$HUD.show_message("Get Ready")
	$Music.play()

func _ready():
	pass

func _on_mob_timer_timeout():
	# Create a new instance of the Mob scene.
	var mob = mob_scene.instantiate()
	
	# Choose a random location on Path2D.
	var mob_spawn_location = get_node("MobPath/MobSpawnLocation")
	mob_spawn_location.progress_ratio = randf()
	
	# Set the mob's position to a random location.
	mob.position = mob_spawn_location.position
	
	# Set the mob's rotation to face the bottom of the window
	var direction = PI / 2
	mob.rotation = direction
	
	# Choose the velocity for the mob.
	var velocity = Vector2(randf_range(150.0, 250.0), 0.0)
	mob.linear_velocity = velocity.rotated(direction)
	
	# Spawn the mob by adding it to the Main scene.
	add_child(mob)

func _on_start_timer_timeout():
	$MobTimer.start()

func _on_player_fire():
	if bullet_cooldown:
		return
	bullet_cooldown = true
	$BulletTimer.start()
	var bullet = bullet_scene.instantiate()
	bullet.position = $Player.position
	bullet.position.y -= 20
	bullet.bullet_hit.connect(_on_bullet_hit)
	add_child(bullet)

func _on_bullet_hit(mob):
	mob.hide()
	score += 1
	$HUD.update_score(score)

func _on_bullet_timer_timeout():
	bullet_cooldown = false
